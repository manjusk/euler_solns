def is_palindrome(num):
  return str(num) == str(num)[::-1]

def palindromes(st, lim):
  pal = []
  for i in range(lim, st, -1):
    for j in range(lim, st, -1):
      if is_palindrome(i * j):
        pal.append(i * j)
  return pal

def largest_pal(pal):
  return max(pal)

print(largest_pal(palindromes(100, 999)))

#or

def pal_lst(st, lim):
  return max([(i * j) for i in range(lim, st, -1) for j in range(lim, st, -1) if is_palindrome(i * j)])

print(pal_lst(100, 999))
