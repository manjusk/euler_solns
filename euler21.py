import sys
def factors(n):
    factors_lst = [1]
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            factors_lst.append(i) 
            factors_lst.append(n // i)
    return set(factors_lst)

def sum_of_factors(num):
    return sum(factors(num))

def is_amicable(num):
    return sum_of_factors(sum_of_factors(num)) == num and num != sum_of_factors(num)

def list_amicables(lim):
    amic = []
    for i in range(1, lim + 1):
        if is_amicable(i):
            amic.extend((i, sum_of_factors(i)))
    return sum(set(amic))

print(sys.argv)
print(list_amicables(int(sys.argv[1])))
