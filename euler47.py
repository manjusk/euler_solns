import sys
from typing import List
from sympy import isprime

def factors(num: int)-> List:
    factors_lst = [num]
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            factors_lst.append(i)
            factors_lst.append(num // i)
    return [i for i in set(factors_lst) if isprime(i)]

def nums_with_n_prime_factors(n: int, lim: int)-> List:
    factors_dict = {}
    for i in range(100000, lim):
        factors_dict[i] = factors(i)
    return sorted({i for i, j in factors_dict.items() if len(j) == n})

def consecutive_distinct_prime_factor_containers(n: int, lim: int):
    n_prime_factor_containers = nums_with_n_prime_factors(n , lim)
    consecutive_count = 0
    consecutive_lst = []
    for i in range(len(n_prime_factor_containers)):
        consecutive_lst += [n_prime_factor_containers[i]]
        if not n_prime_factor_containers[i + 1] == n_prime_factor_containers[i] + 1:
            consecutive_count = 1
            consecutive_lst = []
        else:
            consecutive_count += 1
            consecutive_lst.append(n_prime_factor_containers[i + 1])
            if consecutive_count == n:
                return consecutive_lst[0]
    return -1

print(consecutive_distinct_prime_factor_containers(int(sys.argv[1]), int(sys.argv[2])))

