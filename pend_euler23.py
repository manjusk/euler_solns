import sys
def sum_of_factors(num):
    fact_set = {1} 
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            fact_set.add(i)
            fact_set.add(num // i)
    return sum(fact_set)

print(sum_of_factors(28))

def is_abundant(num):
    if num < 1:
        return False
    return sum_of_factors(num) > num

def make_abundant_lst(lim):
    ab_lst, i = [], 0
    while i < lim:
        if is_abundant(i):
            ab_lst.append(i)
        i += 1
    return ab_lst

print(make_abundant_lst(int(sys.argv[1])))


