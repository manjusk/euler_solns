import sys
import time
DIVS = [11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

def is_div(num):
    for div in DIVS:
        if num % div != 0:
            return False
    return True

def smallest_mul(lim):
    for num in range(1, lim):
        if num % 2 != 0:
            pass
        elif is_div(num):
            return num
    return -1

st = time.time()
print(smallest_mul(int(sys.argv[1])))
print(time.time() - st)
