def sort_file(file_n, mode):
    with open(file_n, mode) as f:
        return sorted([ch.strip('""') for ch in [x for x in f.read().split(",")]])

print(sort_file("names.txt", "r"))

def alpha_val_calc(file_n, mode):
    alpha_val = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "I": 9, "J": 10, "K": 11, "L": 12, "M": 13, "N": 14, "O": 15, "P": 16, "Q": 17, "R": 18, "S": 19, "T": 20, "U": 21, "V": 22, "W": 23, "X": 24, "Y": 25, "Z": 26}
    names = sort_file(file_n, mode)
    val_list = []
    for name in names:
        if '"' in name:
            name = name.replace('"', "")
        name = name.rstrip()
        val = 0
        for ch in name:
            val += alpha_val[ch]
        val_list.append(val)
    return val_list

def sum_multiply(file_n, mode):
    return sum([i * v for i, v in enumerate(alpha_val_calc(file_n, mode), 1)])

print(sum_multiply("names.txt", "r"))
