def multiples_of_3n5(limit: int) -> int:
    return sum([x for x in range(limit) if x % 3 == 0 or x % 5 == 0])

print(multiples_of_3n5(1000))
