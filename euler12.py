import sys

def count_divisors(num):
    divs = [1, num]
    for i in range(2, int(num ** 0.5)):
        if num % i == 0:
            divs.append(i)
            divs.append(num % i)
    return len(divs)

def triangle_nums(lim):
    i, j = 1, 2
    lim += 1
    while i < lim:
        i += j
        j += 1
        if count_divisors(i) >= 500:
            return i
    return -1

print(triangle_nums(int(sys.argv[1])))
