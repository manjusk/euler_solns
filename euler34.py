START = 3
def factorial(num):
    if num < 2:
        return 1
    return num * factorial(num - 1)

def is_curious(num):
    return sum([factorial(int(i)) for i in str(num)]) == num

def curious(lim):
    return sum([n for n in range(START, lim) if is_curious(n)])

import sys
print(curious(int(sys.argv[1])))

