def is_prime(num):
    if num < 2:
        return False
    if num in [2, 3, 5, 7]:
        return True
    if num % 2 == 0:
        return False
    r = 3
    while r <= num ** 0.5:
        if num % r == 0:
            return False
        r += 2
    return True

def rotate(num):
    rotations, i = [], 0
    while i < len(str(num)):
        num = str(num)[1:] + str(num)[0]
        rotations.append(num)
        i += 1
    return rotations

def circularPrimesCount(lim):
    count, i = 0, 0
    while i < lim:
        if is_prime(i):
            if all(is_prime(int(j)) for j in rotate(i)):
                count += 1
        i += 1
    return count

import sys
print(circularPrimesCount(int(sys.argv[1])))
