START = 10
def is_prime(num):
    if num < 2:
        return False
    if num in [2, 3, 5, 7]:
        return True
    if num % 2 == 0:
        return False
    res = 3
    while res <= num ** 0.5:
        if num % res == 0:
            return False
        res += 2
    return True

def right_trunc_prime(num):
    while num > 0:
        if not is_prime(num):
            return False
        num //= 10
#        print(num)
    return True

#print(right_trunc_prime(11))

def left_trunc_prime(num):
    size = len(str(num))
    while num > 0:
        if not is_prime(num):
            return False
        num %= 10 ** (size - 1)
        size -= 1
#        print(num)
    return True

#print(left_trunc_prime(11))

def sum_trunc_primes(limit):
    total = 0
    count = 0
    limit += 1
    for i in range(START, limit):
        if left_trunc_prime(i) and right_trunc_prime(i):
            total += i
            count += 1
            print(i, end = "  ")
    return total, count

import sys
#print(right_truncatable_prime(int(sys.argv[1])))
#print(left_truncatable_prime(int(sys.argv[1])))
print(sum_trunc_primes(int(sys.argv[1])))

