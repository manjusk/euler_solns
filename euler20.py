def fact(num):
  if num <= 1:
    return 1
  return num * fact(num - 1)

def add(num):
  total = 0
  while num > 0:
    total += num % 10
    num //= 10
  return total

print(add(fact(100)))
