import sys
PWR = 5
print(sys.argv)
def digit_powers(i)-> int:
    return sum([int(j) ** PWR for j in str(i)])

def isSame(num)-> bool:
    return digit_powers(num) == num

def check_validity(lim):
    total = 0
    for i in range(10, lim):
        if isSame(i):
            total += i
    return total

import sys
print(check_validity(int(sys.argv[1])))
