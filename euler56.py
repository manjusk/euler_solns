import sys

def digit_sum(num):
    dig_val = 0
    while num > 0:
        dig_val += num % 10
        num //= 10
    return dig_val

def make_power_lst(num, pwr):
    pwr_lst = []
    for i in range(2, num + 1):
        for j in range(1, pwr + 1):
            pwr_lst.append(i ** j)
    return pwr_lst

def max_digit_sum(num, pwr):
    return max([digit_sum(i) for i in make_power_lst(num, pwr)])

print(max_digit_sum(int(sys.argv[1]), int(sys.argv[2])))
