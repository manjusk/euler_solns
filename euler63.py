import sys

def count_pow_dig(max_size):
    count = 0
    for i in range(1, max_size):
        power = 1
        while True:
            if power == len(str(i ** power)):
                count += 1
            else:
                break
            power += 1
    return count

print(count_pow_dig(int(sys.argv[1])))
