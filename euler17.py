def convert2digits(num: int) -> str:
    upto19 = ["", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ",               "Eight ","Nine ", "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",           "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "]
    tens = ["", "", "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
           "Seventy ", "Eighty ", "Ninety "]
    if num < 20:
        return upto19[num]
    else:
        return tens[num // 10] + upto19[num % 10]

def convert3digits(num: int) -> str:
    if num < 100:
        return convert2digits(num)
    hs = num // 100
    tu = num % 100
    if tu > 0:
        connect = "Hundred And "
    else:
        connect = "Hundred "
    return convert2digits(hs) + connect + convert2digits(tu)

#print(convert3digits(245))

def split_into(units: [int], num: int) -> [int]:
    splits = []
    for unit in units:
        splits.append((num // unit, unit))
        num %= unit
    return splits

indian = {10**7: "Crore ", 10**5: "Lakh ", 1000: "Thousand ", 1:""}

def fig2words(style:dict, num: int) -> str:
    splits = split_into(style.keys(), num)
    xform_units = [(a, style[b]) for a, b in splits]
    xform_values = [convert3digits(a) + b for a, b in xform_units if a > 0]
    return ''.join(xform_values)

#print(fig2words(indian, 256))

def num_list(st, lim):
    return [fig2words(indian, i) for i in range(st, lim + 1)]

def count_letters(st, lim):
    count = 0
    words = num_list(st, lim)
    for chars in words:
        for j in chars:
            if not j.isspace():
                count += 1
    return count

print(count_letters(1, 1000))
