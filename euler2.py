import sys
def fib_seq(lim):
    first, second = 0, 1
    seq = []
    while first < lim:
        seq.append(first)
        first, second = second, second + first
    return seq

print(sys.argv)
def even_fib_sum(limit):
    sequence = fib_seq(limit)
    result = 0
    for n in sequence:
        if n % 2 == 0:
            result += n
    return result

#    return sum([i for i in fib_seq(limit) if i % 2 == 0])

print(even_fib_sum(int(sys.argv[1])))
