def pwr_digit_sum(num, pwr):
  return sum(int(i) for i in str(num ** pwr))

print(pwr_digit_sum(2, 1000))

#Or

def fun(num, pwr):
  total = 0
  res = num ** pwr
  while res > 0:
    t += res % 10
    res //= 10
  return total

print(fun(2, 1000))
