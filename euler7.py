import sys

def is_prime(num):
	if num < 2:
	    return False
	if num in [2, 3, 5, 7]:
		return True
	if num % 2 == 0:
		return False
	r = 3
	while r <= num ** 0.5:
	    if num % r == 0:
		    return False
	    r += 2
	return True

def nth_prime(step):
    pos = 1
    num = 0
    while pos <= step:
        num += 1
       	if is_prime(num):
            pos += 1
    return num

print(nth_prime(int(sys.argv[1])))

