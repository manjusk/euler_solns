def factors(n):
    factors_lst = [1, n]
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            factors_lst.append(i)
            factors_lst.append(n // i)
    return set(factors_lst)

print(factors(100))

def is_prime(num):
    if num < 2:
        return False
    if num in [2, 3, 5, 7]:
        return True
    if num % 2 == 0:
        return False
    r = 3
    while r <= num ** 0.5:
        if num % r == 0:
            return False
        r += 2
    return True

def largest_prime_factor(num):
    factors = factors(num)
    prime_factors = []
    for i in factors:
        if is_prime(i):
            prime_factors.append(i)
    return max(prime_factors)

    #return max([i for i in factors(num) if is_prime(i)])

print(largest_prime_factor(600851475143))
