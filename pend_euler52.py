from typing import List
def n_tuple(num, n_t: List):
    return [num * i for i in n_t]

def is_permuted(num, num2):
    if len(str(num)) != len(str(num2)):
        return False
    return str(num) in str(num2) + str(num2)

def permuted(lst):
    for i, i + 1 in lst:
        print(i)

print(n_tuple(11, [2, 3, 4, 5, 6]))

