HIGHEST = 26

def max_triangle_req(file_n, mode):
    with open(file_n, mode) as f:
        return max([len(w) for w in [ch.strip('""') for ch in [x for x in f.read().split(",")]]]) * HIGHEST

#print(max_triangle_req("words.txt", "r"))

def words_list(file_n, mode):
    with open(file_n, mode) as f:
        return [ch.strip('""') for ch in [x for x in f.read().split(",")]]

def alpha_val_calc(file_n, mode):
    alpha_val = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "I": 9, "J": 10, "K": 11, "L": 12, "M": 13, "N": 14, "O": 15, "P": 16, "Q": 17, "R": 18, "S": 19, "T": 20, "U": 21, "V": 22, "W": 23, "X": 24, "Y": 25, "Z": 26}
    names = words_list(file_n, mode)
    val_list = []
    for name in names:
        val = 0
        for ch in name:
            val += alpha_val[ch]
        val_list.append(val)
    return val_list

def triangle_nums(lim):
    lst = []
    i, j = 1, 2
    lim += 1
    while i < lim:
        lst.append(i)
        i += j
        j += 1
    return lst

def count_triangle_words(func1, func2):
    count = 0
    for i in func1:
        if i in func2:
            count += 1
    return count

print(count_triangle_words(alpha_val_calc("words.txt", "r"), triangle_nums(max_triangle_req("words.txt", "r"))))
