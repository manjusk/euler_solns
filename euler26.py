import sys
def fraction_to_decimal(numr, denr):
    res = ""
    mp = {}
    rem = numr % denr
    while rem != 0 and rem not in mp:
        mp[rem] = len(res)
        rem *= 10
        res_part = rem // denr
        res += str(res_part)
        rem %= denr
 
    if rem == 0:
        return ""
    else:
        return res[mp[rem]:]

def longest_rec_cycle(random_val, lim):
    cycle_dict = {len(fraction_to_decimal(random_val, i)): i for i in range(2, lim + 1)}
    return cycle_dict[max(cycle_dict)]

print(longest_rec_cycle(int(sys.argv[1])))
