def add_self_powered(lim, required):
    tot = 0
    for i in range(1, lim + 1):
        tot += i ** i
    return str(tot)[-required:]

import sys
print(add_self_powered(int(sys.argv[1]), int(sys.argv[2])))
