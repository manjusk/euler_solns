def pythagoran_triplet(x):
  for m in range(x):
    for n in range(x):
      a = m ** 2 - (n ** 2)
      b = 2 * m * n
      c = m ** 2 + (n ** 2)
      if a + b + c == x and a > 0 and b > 0 and c > 0:
        return a * b * c
      continue

print(pythagoran_triplet(x))
