from typing import List, Dict
from sys import argv
from sympy import isprime
import itertools

def four_dig_primes(st = 1000, lim = 9999):
    return [i for i in range(st, lim + 1) if isprime(i)]

def permutations(st = 1000, lim = 9999):
    primes = four_dig_primes(st, lim)
    perms = []
    for a, b in itertools.combinations(primes, 2):
        if sorted(str(a)) == sorted(str(b)):
            perms.extend((str(a), str(b)))
    return set(perms)

#print(four_dig_primes(1000, 9999))
#print(permutations(1000, 9999))

def remove_single_and_two_element_keys(anagrams: Dict[str, List[str]]) -> Dict[str, List[str]]:
    for key in list(anagrams.keys()):
        if len(anagrams[key]) == 1 or len(anagrams[key]) == 2:
            anagrams.pop(key)
    return anagrams

def get_word_key(word: str) -> str:
    return "".join(sorted(word))

def get_anagrams(words: List[str]) -> Dict[str, List[str]]:
    anagrams = dict()
    for word in words:
        sorted_word = get_word_key(word)
        if sorted_word not in anagrams:
            anagrams[sorted_word] = [word]
        else:
            anagrams[sorted_word].append(word)
    return remove_single_and_two_element_keys(anagrams)

print(get_anagrams(permutations(1000, 9999)))
