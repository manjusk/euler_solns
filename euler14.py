def next_collatz(n: int)->int:
    return n // 2 if n % 2 == 0 else 3 * n + 1

def collatz_seq(num: int)->[int]:
    if num == 4:
        return [4, 2, 1]
    return [num] + collatz_seq(next_collatz(num))

def col_sq_lst(lim):
    lim += 1
    col_dict = {}
    for i in range(1, lim):
        col_dict[len(collatz_seq(i))] = i
    return col_dict[max(col_dict.keys())]

import time
st = time.time()
print(col_sq_lst(1000000))
print(time.time() - st)
