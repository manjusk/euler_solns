def is_prime(num):
  if num < 2:
    return False
  if num in [2, 3, 5, 7]:
    return True
  if num % 2 == 0:
    return False
  r = 3
  while r <= num ** 0.5:
    if num % r == 0:
      return False
    r += 2
  return True

def sum_prime(nums):
  return sum(i for i in range(nums + 1) if is_prime(i))

#Code is taking too much time (*27secs)
import time
s = time.time()
sum_prime(2000000)
print(time.time() - s)


