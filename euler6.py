import sys
print(sys.argv)
def sum_sqr_diff(lim):
  return sum(y for y in range(1, lim + 1)) ** 2 - sum(x ** 2 for x in range(1, lim + 1))

print(sum_sqr_diff(int(sys.argv[1])))
