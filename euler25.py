import sys
def fibo_series(lim):
    a, b = 1, 1
    fib_seq = []
    while a < lim:
        fib_seq.append(len(str(a)))
        a, b = b, a + b
    return fib_seq

def pos_with_n_digits(n, lim):
    series = fibo_series(lim)
    #print(series)
    for i in series:
        if i == n:
            return series.index(i) + 1
    return -1

print(pos_with_n_digits(int(sys.argv[1]), 10 ** 1000))
