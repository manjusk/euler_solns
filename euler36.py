def dec2bin(num):
    #return bin(num)
    binary = ""
    temp = num
    while temp > 0:
        binary += str(temp % 2)
        temp //= 2
    return binary[::-1]

def is_pal(num):
    return str(num) == str(num)[::-1]

def add_pals(lim):
    total = 0
    for num in range(lim):
        if is_pal(num) and is_pal(dec2bin(num)):
            total += num
    return total

import sys
print(add_pals(int(sys.argv[1])))
