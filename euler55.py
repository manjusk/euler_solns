def is_pal(num):
    return str(num) == str(num)[::-1]

def add_with_reverse(num):
    rev = 0
    temp = num
    size = len(str(num)) - 1
    while temp > 0:
        rem = temp % 10
        rev += rem * (10 ** size)
        size -= 1
        temp //= 10
    return num + rev

def is_lychrel(num, max_iters):
    iters = 0
    while iters < max_iters:
        num = add_with_reverse(num)
        if is_pal(num):
            return False
        iters += 1
    return True

def count_lychrel(lim, max_iters):
    lychrel = 0
#    lychrel_lst = []
    for i in range(lim + 1):
        if is_lychrel(i, max_iters): 
            lychrel += 1
#            lychrel_lst.append(i)
    return lychrel, #lychrel_lst

import sys
print(count_lychrel(int(sys.argv[1]), int(sys.argv[2])))


